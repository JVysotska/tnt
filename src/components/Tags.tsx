import { icons } from "../theme/icons/tag";

export const Tags = () => {
    return (
        <div className="tags">
            <span className="tag">
                <icons.React/> React
            </span>
        </div>
    )
};