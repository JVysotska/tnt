import { icons } from "../../theme/icons/tag";
import { Tip } from "./Tip";

export const TipList = () => {
    return (
        <section className="tip-list">
            <Tip />
        </section>
    )
};