/* Core */
import { Routes, Route, Outlet, Navigate } from 'react-router-dom';

/* Components */
import { HomePage } from './pages';

export const App = () => {
    return (
        <>
           <HomePage/>
        </>
    );
};
